package com.earth.kotlinandroidtutoria

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShowNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_name)
       val name:String = intent.getStringExtra( "name")?: "Unknown"
        val txtName = findViewById<TextView>(R.id.txtName)
        txtName.text = name
    }
}